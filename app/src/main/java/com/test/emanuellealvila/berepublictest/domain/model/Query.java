package com.test.emanuellealvila.berepublictest.domain.model;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class Query {

    private QueryParams lastQuery;
    private QueryParams newQuery;

    public Query(QueryParams lastQuery, QueryParams newQuery) {
        this.lastQuery = lastQuery;
        this.newQuery = newQuery;
    }

    public QueryParams getLastQuery() {
        return lastQuery;
    }

    public QueryParams getNewQuery() {
        return newQuery;
    }

    public void setLastQuery(QueryParams lastQuery) {
        this.lastQuery = lastQuery;
    }

    public void setNewQuery(QueryParams newQuery) {
        this.newQuery = newQuery;
    }

    public boolean equals(){
        return lastQuery.getTerm().equals(newQuery.getTerm());
    }
}
