package com.test.emanuellealvila.berepublictest.app.utils;

/**
 * Created by Emanuel on 21/02/2016.
 *
 * Class which defines constants to use like params in rest requests
 */
public class ParamKeys {

    public static final String TERM = "term";
    public static final String MEDIA = "media";
    public static final String ENTITY = "entity";
}
