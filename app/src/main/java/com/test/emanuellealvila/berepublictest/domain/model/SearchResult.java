package com.test.emanuellealvila.berepublictest.domain.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Emanuel on 20/02/2016.
 */
public class SearchResult {

    private int resultCount;
    private List<ItunesObject> results = new ArrayList<ItunesObject>();

    public int getResultCount() {
        return resultCount;
    }

    public List<ItunesObject> getResults() {
        return results;
    }
}
