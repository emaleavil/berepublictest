package com.test.emanuellealvila.berepublictest.app.adapter;

import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.test.emanuellealvila.berepublictest.R;
import com.test.emanuellealvila.berepublictest.app.BeRepublicApplication;
import com.test.emanuellealvila.berepublictest.domain.model.ItunesObject;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class ResultsAdapter extends RecyclerView.Adapter {

    private List<ItunesObject> items;

    public ResultsAdapter(SearchResult items){
        this.items = items.getResults();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView layout = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.itunes_item_layout, parent, false);
        return new RecyclerHolder(layout);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        final ItunesObject item = items.get(position);
        RecyclerHolder myHolder = (RecyclerHolder) holder;

        myHolder.title.setText(item.getTrackName());
        myHolder.author.setText(item.getArtistName());
        myHolder.releaseDate.setText(item.getReleaseDate());
        myHolder.album.setText(item.getCollectionName());

        Glide.with(BeRepublicApplication.getInstance()).load(Uri.parse(item.getArtworkUrl100())).into(myHolder.picture);


    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public static class RecyclerHolder extends RecyclerView.ViewHolder{

        @Bind(R.id.author)
        TextView author;
        @Bind(R.id.title)
        TextView title;
        @Bind(R.id.album)
        TextView album;
        @Bind(R.id.release_date)
        TextView releaseDate;
        @Bind(R.id.album_picture)
        ImageView picture;

        public RecyclerHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
