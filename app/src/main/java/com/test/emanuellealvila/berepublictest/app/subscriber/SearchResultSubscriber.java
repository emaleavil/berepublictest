package com.test.emanuellealvila.berepublictest.app.subscriber;

import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;
import com.test.emanuellealvila.berepublictest.presentation.MainPresenter;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 *
 * Subscriber who receives the search results from network
 */
public class SearchResultSubscriber implements Observer<SearchResult>{


    MainPresenter presenter;

    public SearchResultSubscriber(MainPresenter presenter){
        this.presenter = presenter;
    }

    @Override
    public void onCompleted() {
    }

    @Override
    public void onError(Throwable e) {
        presenter.showError();
    }

    @Override
    public void onNext(SearchResult searchResult) {
        presenter.showResults(searchResult);
    }
}
