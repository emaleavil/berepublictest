package com.test.emanuellealvila.berepublictest.app.di.modules;


import com.test.emanuellealvila.berepublictest.app.activities.MainActivity;
import com.test.emanuellealvila.berepublictest.app.subscriber.SearchResultSubscriber;
import com.test.emanuellealvila.berepublictest.app.subscriber.StoreSubscriber;
import com.test.emanuellealvila.berepublictest.app.view.MainView;
import com.test.emanuellealvila.berepublictest.domain.interactors.SearchAction;
import com.test.emanuellealvila.berepublictest.domain.interactors.StoreAction;
import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.domain.model.QueryParams;
import com.test.emanuellealvila.berepublictest.presentation.MainPresenter;
import com.test.emanuellealvila.berepublictest.presentation.MainPresenterImpl;
import com.test.emanuellealvila.berepublictest.repository.Repository;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 7/12/15.
 */
@Module
public class MainModule {

    private MainActivity mainActivity;

    public MainModule(MainActivity activity){
        this.mainActivity = activity;
    }

    @Provides
    public MainView provideView(){
        return mainActivity;
    }

    @Provides
    public MainPresenter providePresenter(SearchAction searchAction, StoreAction storeAction){
        return new MainPresenterImpl(mainActivity, searchAction, storeAction);
    }

    @Provides
    public SearchAction provideSearchAction(Repository repository){
        return new SearchAction(repository);
    }

    @Provides
    public StoreAction provideStoreAction(Repository repo){
        return new StoreAction(repo);
    }

    @Provides
    public QueryParams provideParams(){
        return new QueryParams();
    }

    @Provides
    public SearchResultSubscriber provideSearchSubscriber(MainPresenter presenter){
        return new SearchResultSubscriber(presenter);
    }
    @Provides
    public StoreSubscriber provideStoreSubscriber(MainPresenter presenter){
        return new StoreSubscriber(presenter);
    }

    @Provides
    public Query provideQuery(QueryParams lastQuery, QueryParams newQuery){
        return new Query(lastQuery, newQuery);
    }
}

