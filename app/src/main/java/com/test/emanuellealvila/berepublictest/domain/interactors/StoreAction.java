package com.test.emanuellealvila.berepublictest.domain.interactors;

import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;
import com.test.emanuellealvila.berepublictest.repository.Repository;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class StoreAction implements Invoker {

    private Repository repo;
    private SearchResult result;

    public StoreAction(Repository repo) {
        this.repo = repo;
    }

    public void setData(SearchResult result){
        this.result = result;
    }

    @Override
    public void execute(Observer subscriber) {
        repo.store(result,subscriber);
    }
}
