package com.test.emanuellealvila.berepublictest.domain.model;

/**
 * Created by Emanuel on 20/02/2016.
 */
public class ItunesObject {

    private long artistId;
    private long collectionId;

    private String wrapperType;
    private String kind;
    private String artistName;
    private String collectionName;
    private String trackName;
    private String collectionCensodedName;
    private String collectionArtistName;
    private String previewUrl;
    private String artworkUrl30;
    private String artworkUrl60;
    private String artworkUrl100;
    private String releaseDate;
    private String currency;
    private String primaryGenreName;

    private float collectionPrice;
    private float trackPrice;


    public long getArtistId() {
        return artistId;
    }

    public long getCollectionId() {
        return collectionId;
    }

    public String getWrapperType() {
        return wrapperType;
    }

    public String getKind() {
        return kind;
    }

    public String getArtistName() {
        return artistName;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public String getTrackName() {
        return trackName;
    }

    public String getCollectionCensodedName() {
        return collectionCensodedName;
    }

    public String getCollectionArtistName() {
        return collectionArtistName;
    }

    public String getPreviewUrl() {
        return previewUrl;
    }

    public String getArtworkUrl30() {
        return artworkUrl30;
    }

    public String getArtworkUrl60() {
        return artworkUrl60;
    }

    public String getArtworkUrl100() {
        return artworkUrl100;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public String getCurrency() {
        return currency;
    }

    public String getPrimaryGenreName() {
        return primaryGenreName;
    }

    public float getCollectionPrice() {
        return collectionPrice;
    }

    public float getTrackPrice() {
        return trackPrice;
    }
}
