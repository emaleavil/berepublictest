package com.test.emanuellealvila.berepublictest.app.di.modules;

import android.content.Context;

import com.test.emanuellealvila.berepublictest.datasources.FakeDatabase;
import com.test.emanuellealvila.berepublictest.datasources.FakeDatabaseImpl;
import com.test.emanuellealvila.berepublictest.datasources.RepositoryImpl;
import com.test.emanuellealvila.berepublictest.repository.Repository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by emanuel on 16/02/16.
 */
@Singleton
@Module
public class ApplicationModule {

    private Context appContext;

    public ApplicationModule(Context appContext){
        this.appContext = appContext;
    }

    @Provides
    @Singleton
    public Context provideAppContext(){
        return appContext;
    }

    @Provides @Singleton
    Repository provideRepository(FakeDatabase database){
        return new RepositoryImpl(database);
    }

    @Provides
    FakeDatabase provideDatabase(){
        return new FakeDatabaseImpl();
    }
}
