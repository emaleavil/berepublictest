package com.test.emanuellealvila.berepublictest.presentation;

import com.test.emanuellealvila.berepublictest.app.view.MainView;
import com.test.emanuellealvila.berepublictest.domain.interactors.SearchAction;
import com.test.emanuellealvila.berepublictest.domain.interactors.StoreAction;
import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class MainPresenterImpl implements MainPresenter{

    private MainView view;
    private SearchAction searchAction;
    private StoreAction storeAction;


    public MainPresenterImpl(MainView view, SearchAction searchAction, StoreAction storeAction){
        this.view = view;
        this.searchAction = searchAction;
        this.storeAction = storeAction;
    }

    @Override
    public void search(Query query, Observer subscriber) {
        view.showDialogSearching();
        searchAction.setQuery(query);
        searchAction.execute(subscriber);
    }

    @Override
    public void store(SearchResult result, Observer subscriber) {
        storeAction.setData(result);
        storeAction.execute(subscriber);
    }

    @Override
    public void showResults(SearchResult results) {
        if(results.getResultCount() != 0){
            view.showSearchResults(results);
        }else{
            view.showEmptyView();
        }
    }

    @Override
    public void showError() {
        view.showError();
    }

}
