package com.test.emanuellealvila.berepublictest.app.di.components;

import android.content.Context;

import com.test.emanuellealvila.berepublictest.app.di.modules.ApplicationModule;
import com.test.emanuellealvila.berepublictest.datasources.FakeDatabase;
import com.test.emanuellealvila.berepublictest.repository.Repository;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@Singleton
@Component(modules =
        {
                ApplicationModule.class
        })
public interface ApplicationComponent {

    void inject(Context context);

    Context context();
    Repository getRepository();
    FakeDatabase getDatabase();
}
