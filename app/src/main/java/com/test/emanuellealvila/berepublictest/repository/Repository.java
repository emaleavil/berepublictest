package com.test.emanuellealvila.berepublictest.repository;

import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 */
public interface Repository {

    void search(Query query, Observer subscriber);
    void store(SearchResult data, Observer subscriber);


}
