package com.test.emanuellealvila.berepublictest.app;

import android.app.Application;

import com.test.emanuellealvila.berepublictest.app.di.components.ApplicationComponent;
import com.test.emanuellealvila.berepublictest.app.di.components.DaggerApplicationComponent;
import com.test.emanuellealvila.berepublictest.app.di.modules.ApplicationModule;


/**
 * Created by emanuel on 16/02/16.
 */
public class BeRepublicApplication extends Application {

    private static BeRepublicApplication sInstance = null;

    private ApplicationComponent component;

    @Override
    public void onCreate() {
        super.onCreate();
        sInstance = this;

        component  = DaggerApplicationComponent.builder().applicationModule(new ApplicationModule(this)).build();
        component.inject(sInstance);
    }

    public static BeRepublicApplication getInstance(){
        return sInstance;
    }

    public ApplicationComponent getApplicationComponent(){
        return component;
    }

}
