package com.test.emanuellealvila.berepublictest.app.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.test.emanuellealvila.berepublictest.R;
import com.test.emanuellealvila.berepublictest.app.BeRepublicApplication;
import com.test.emanuellealvila.berepublictest.app.adapter.ResultsAdapter;
import com.test.emanuellealvila.berepublictest.app.di.components.DaggerMainComponent;
import com.test.emanuellealvila.berepublictest.app.di.modules.MainModule;
import com.test.emanuellealvila.berepublictest.app.subscriber.SearchResultSubscriber;
import com.test.emanuellealvila.berepublictest.app.subscriber.StoreSubscriber;
import com.test.emanuellealvila.berepublictest.app.view.MainView;
import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.domain.model.QueryParams;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;
import com.test.emanuellealvila.berepublictest.presentation.MainPresenter;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements MainView, SearchView.OnQueryTextListener {


    @Bind(R.id.parent)
    LinearLayout parent;
    @Bind(R.id.search_results)
    RecyclerView searchResultsView;
    @Bind(R.id.empty_view)
    RelativeLayout emptyView;
    @Bind(R.id.not_data_found_view)
    RelativeLayout notDataFoundView;


    @Inject
    MainPresenter presenter;
    @Inject
    Query query;
    @Inject
    QueryParams lastQuery;
    @Inject
    QueryParams newQuery;

    @Inject
    SearchResultSubscriber searchSubscriber;
    @Inject
    StoreSubscriber storeSubscriber;


    private MenuItem searchItem;
    private ResultsAdapter adapter;

    private ProgressDialog searchingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initButterknife();
        initDagger();

    }

    private void initButterknife() {
        ButterKnife.bind(this);
    }

    private void initDagger() {
        DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .applicationComponent(BeRepublicApplication.getInstance().getApplicationComponent())
                .build()
                .inject(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);

        searchItem = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        if (searchView != null) {
            searchView.setOnQueryTextListener(this);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void showError() {
        hideDialog();
        String errorMessage = getString(R.string.error_msg);
        Snackbar snackbar = Snackbar.make(parent, errorMessage, Snackbar.LENGTH_SHORT);
        snackbar.show();

    }

    @Override
    public void showSearchResults(SearchResult objects) {
        presenter.store(objects,storeSubscriber);

        emptyView.setVisibility(View.GONE);
        notDataFoundView.setVisibility(View.GONE);
        searchResultsView.setVisibility(View.VISIBLE);
        adapter = new ResultsAdapter(objects);
        LinearLayoutManager manager = new LinearLayoutManager(this);
        searchResultsView.setHasFixedSize(true);
        searchResultsView.setLayoutManager(manager);
        searchResultsView.setAdapter(adapter);

        hideDialog();
    }

    private void hideDialog() {
        if (searchingDialog != null) {
            searchingDialog.dismiss();
        }
    }

    @Override
    public void showEmptyView() {
        hideDialog();
        emptyView.setVisibility(View.GONE);
        searchResultsView.setVisibility(View.GONE);
        notDataFoundView.setVisibility(View.VISIBLE);
    }

    @Override
    public void showDialogSearching() {
        searchingDialog = ProgressDialog.show(this, getString(R.string.dialog_title),
                getString(R.string.body_dialog), true);
        searchingDialog.show();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        try {
            newQuery.resetQuery();
            newQuery.setTerm(query);
            QueryParams params = (QueryParams) newQuery.clone();

            this.query.setLastQuery(lastQuery);
            this.query.setNewQuery(newQuery);


            presenter.search(this.query, searchSubscriber);
            lastQuery = params;


            MenuItemCompat.collapseActionView(searchItem);

        } catch (CloneNotSupportedException e) {
            showError();
        }

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return lastQuery.equals(newText);
    }
}
