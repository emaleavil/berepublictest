package com.test.emanuellealvila.berepublictest.app.di.components;


import com.test.emanuellealvila.berepublictest.app.activities.MainActivity;
import com.test.emanuellealvila.berepublictest.app.di.PerActivity;
import com.test.emanuellealvila.berepublictest.app.di.modules.MainModule;

import dagger.Component;

/**
 * Created by emanuel on 7/12/15.
 */

@PerActivity
@Component(dependencies = {ApplicationComponent.class}, modules = {MainModule.class})
public interface MainComponent {
    void inject(MainActivity mainActivity);
}
