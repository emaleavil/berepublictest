package com.test.emanuellealvila.berepublictest.presentation;

import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 */
public interface MainPresenter {

    void search(Query query, Observer subscriber);
    void store(SearchResult result,  Observer subscriber);
    void showResults(SearchResult results);
    void showError();
}
