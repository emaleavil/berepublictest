package com.test.emanuellealvila.berepublictest.datasources;

import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.domain.model.QueryParams;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;
import com.test.emanuellealvila.berepublictest.repository.Repository;

import javax.inject.Inject;

import rx.Observable;
import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Emanuel on 21/02/2016.
 * <p>
 * This class handles data retrieving from local storage or network.
 */
public class RepositoryImpl implements Repository {


    RestApi restApi;
    FakeDatabase database;

    @Inject
    public RepositoryImpl(FakeDatabase database) {
        restApi = Service.getService();
        this.database = database;
    }


    @Override
    public void search(Query query, Observer subscriber) {
        Observable<SearchResult> observable = null;
        if (query.equals()) {
            observable = Observable.just(database.getData());


        } else {
            QueryParams newQuery = query.getNewQuery();
            observable = restApi.search(newQuery.getTerm(), newQuery.getMedia(), newQuery.getEntity());

        }

        observable.observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.newThread())
                .subscribe(subscriber);

    }

    @Override
    public void store(SearchResult data, Observer subscriber) {
        database.storeData(data);
    }


}
