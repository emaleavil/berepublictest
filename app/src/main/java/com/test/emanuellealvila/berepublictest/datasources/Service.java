package com.test.emanuellealvila.berepublictest.datasources;

import com.squareup.okhttp.OkHttpClient;

import retrofit.GsonConverterFactory;
import retrofit.Retrofit;
import retrofit.RxJavaCallAdapterFactory;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class Service {

    public static final String BASE_URL = "https://itunes.apple.com/";

    public static RestApi getService(){

        OkHttpClient client = new OkHttpClient();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        return  retrofit.create(RestApi.class);
    }

}

