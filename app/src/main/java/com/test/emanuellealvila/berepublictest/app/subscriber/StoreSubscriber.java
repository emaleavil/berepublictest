package com.test.emanuellealvila.berepublictest.app.subscriber;

import com.test.emanuellealvila.berepublictest.presentation.MainPresenter;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class StoreSubscriber implements Observer{

    private MainPresenter presenter;

    public StoreSubscriber(MainPresenter presenter){
        this.presenter = presenter;
    }
    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(Object o) {

    }
}
