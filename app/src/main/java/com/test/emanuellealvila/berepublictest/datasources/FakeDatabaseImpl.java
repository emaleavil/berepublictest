package com.test.emanuellealvila.berepublictest.datasources;

import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class FakeDatabaseImpl implements FakeDatabase{

    private static SearchResult data = new SearchResult();

    public FakeDatabaseImpl(){}

    @Override
    public SearchResult getData() {
        return data;
    }

    @Override
    public void storeData(SearchResult data) {
        this.data = data;
    }
}
