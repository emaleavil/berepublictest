package com.test.emanuellealvila.berepublictest.domain.model;

/**
 * Created by Emanuel on 21/02/2016.
 *
 * Class to send query params to the request. This implementation only uses one params but
 * we can add more params to use in our query.
 */
public class QueryParams implements Cloneable {

    private String term;

    public QueryParams(){
        resetQuery();
    }

    /**
     * This method is used to reset query. At this moment, it's useless but when we have
     * more than one param will be useful
     */
    public void resetQuery(){
        this.term = "";
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }


    /*
    * This methods are used to only return and search m
    */
    public String getMedia(){
        return "music";
    }

    public String getEntity(){
        return "musicTrack";
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
