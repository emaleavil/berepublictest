package com.test.emanuellealvila.berepublictest.domain.interactors;

import com.test.emanuellealvila.berepublictest.domain.model.Query;
import com.test.emanuellealvila.berepublictest.repository.Repository;

import rx.Observer;

/**
 * Created by Emanuel on 21/02/2016.
 */
public class SearchAction implements Invoker{

    private Repository repository;
    private Query query;

    public SearchAction(Repository repository){
        this.repository = repository;
    }

    public void setQuery(Query query){
        this.query = query;
    }

    @Override
    public void execute(Observer subscriber) {
        repository.search(query, subscriber);
    }
}
