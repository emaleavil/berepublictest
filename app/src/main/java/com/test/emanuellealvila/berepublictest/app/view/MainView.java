package com.test.emanuellealvila.berepublictest.app.view;

import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

/**
 * Created by Emanuel on 21/02/2016.
 */
public interface MainView {

    void showError();
    void showSearchResults(SearchResult objects);
    void showEmptyView();
    void showDialogSearching();
}
