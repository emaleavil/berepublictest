package com.test.emanuellealvila.berepublictest.datasources;

import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

/**
 * Created by Emanuel on 21/02/2016.
 */
public interface FakeDatabase {

    SearchResult getData();
    void storeData(SearchResult data);
}
