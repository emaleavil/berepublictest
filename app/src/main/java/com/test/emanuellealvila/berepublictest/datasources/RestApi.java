package com.test.emanuellealvila.berepublictest.datasources;

import com.test.emanuellealvila.berepublictest.app.utils.ParamKeys;
import com.test.emanuellealvila.berepublictest.domain.model.SearchResult;

import retrofit.http.GET;
import retrofit.http.Query;
import rx.Observable;

/**
 * Created by Emanuel on 21/02/2016.
 */
public interface RestApi {

    @GET("search")
    Observable<SearchResult> search(@Query(ParamKeys.TERM) String term,@Query(ParamKeys.MEDIA) String media,@Query(ParamKeys.ENTITY) String entity);

}
